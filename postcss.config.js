/*const tailwind = require('tailwindcss');
const cssnano = require('cssnano');
const postcssImport = require('postcss-import');
const presetEnv = require('postcss-preset-env')({
  features: {
    'nesting-rules': true, // Optional, not necessary. Read details about it  [here](https://tabatkins.github.io/specs/css-nesting/#motivation) 
  },
});

const plugins =
  process.env.NODE_ENV === 'production'
    ? [postcssImport, tailwind, presetEnv, cssnano]
    : [postcssImport, tailwind, presetEnv];

module.exports = { plugins };*/

const tailwindcss = require("tailwindcss"); 

// требуется, только если вы хотите очистить 
const purgecss = require("@fullhuman/postcss-purgecss") ({ 
  content: ["./src/**/*.svelte", "./src/**/*.html"], 
  defaultExtractor: content => content.match (/[A-Za-z0-9-_:/]+/g) || [] }); 

module.exports = { 
  plugins: [ 
    tailwindcss ("./tailwind.js"), 

    // требуется, только если вы хотите очистить 
    ...(process.env.NODE_ENV === "production" ? [purgecss] : [])
  ] 
};