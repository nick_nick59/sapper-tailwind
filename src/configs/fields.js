export default {
  cardFields: [
    {
      key: 'name',
      title: 'Имя'
    }, 
    {
      key: 'username',
      title: 'Псевдоним'
    }, 
    {
      key: 'email',
      title: 'Эл. почта'
    }, 
    {
      key: 'phone',
      title: 'Телефон'
    }, 
    {
      key: 'website',
      title: 'Сайт'
    }, 
    /*{
      key: 'address',
      title: 'Адрес("город", "геоданные", "улица", "офис", "почт.индекс")',
      items: ['city', 'geo', 'street', 'suite', 'zipcode']
    }, 
    {
      key: 'company',
      title: 'Компания("название", "деятельность", "девиз")',
      items: ['name', 'bs', 'catchPhrase']
    },*/
  ]
}