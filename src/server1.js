import sirv from 'sirv';
import compression from 'compression';
import adonis from 'adonis';
import * as sapper from '@sapper/server';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

adonis() // You can also use Express
.use(
    compression({ threshold: 0 }),
    sirv('static', { dev }),
    sapper.middleware()
)
.listen(PORT, err => {
    if (err) console.log('error', err);
});
