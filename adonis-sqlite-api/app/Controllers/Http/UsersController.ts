import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class UsersController {
  private static users = [
    {
      id: 1, 
      name: 'Николай Коваленко', 
      username: 'lom', 
      email: 'kovalenkonikola@yandex.ru',
      phone: '+7-928-191-56-46',
      website: 'Vit-Ko.ru'
    }, 
    {
      id: 2, 
      name: 'Николай Николаевич Коваленко', 
      username: 'big', 
      email: 'knnaksay@gmail.com',
      phone: '+7-123-45-67',
      website: 'Nick-Nick.ru'    
    }
  ]  
  public async index() {
    //return UsersController.users
    return await User.all()
  }
  public async store ({ request }: HttpContextContract) {
    const data = request.only(['name', 'username', 'email', 'phone', 'website'])
    const newId = UsersController.users.length + 1
    const user = {
        id: newId,
        name: data.name,
        username: data.username,
        email: data.email,
        phone: data.phone,
        website: data.website
      }
      //UsersController.users.push(user)
      //return user
      return await User.create(user)
  }
  public async destroy ({ params }: HttpContextContract) {
    //const userId = Number(params.id) //transform to number
    //UsersController.users = UsersController.users.filter(p => p.id !== userId)
    const user = await User.find(params.id)
    user?.delete()
  }
  public async show ({ params }: HttpContextContract) {
    //const userId = Number(params.id)
    //return UsersController.users.find(p => p.id === userId)
    return await User.find(params.id)
  }
}
