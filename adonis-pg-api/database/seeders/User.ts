import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'

export default class UserSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await User.createMany([
      {
        id: 1, 
        name: 'Николай Коваленко', 
        username: 'lom', 
        email: 'kovalenkonikola@yandex.ru',
        phone: '+7-928-191-56-46',
        website: 'Vit-Ko.ru'
      }, 
      {
        id: 2, 
        name: 'Николай Николаевич Коваленко', 
        username: 'big', 
        email: 'knnaksay@gmail.com',
        phone: '+7-123-45-67',
        website: 'Nick-Nick.ru'    
      }
    ])
  }
}
